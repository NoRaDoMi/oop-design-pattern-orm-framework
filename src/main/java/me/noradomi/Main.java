package me.noradomi;


// import me.noradomi.repository.PersonRepository;

import me.noradomi.connection.*;
import me.noradomi.entity.*;
import me.noradomi.mapper.EntityMapper;

import java.io.FileNotFoundException;
import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import java.sql.SQLOutput;
import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) throws IllegalAccessException, InvocationTargetException, SQLException, ClassNotFoundException, InstantiationException, FileNotFoundException {
        LiteConnection liteConnection = new LiteMySQLConnection();
        liteConnection.loadConfig("/home/noradomi/Desktop/oop-design-pattern-orm-framework/conf/development.yaml");
        LiteCommand commandOpen = new OpenConnection(liteConnection);
        LiteCommand commandClose = new CloseConnection(liteConnection);
        commandOpen.execute();

        /** Thao tác select lấy nhiều records **/
//        List<Student> students = liteConnection.select(Student.class).all().getListResults();
//        students.forEach(x -> System.out.println(x.toString()));

        /** Thao tác select lấy 1 record **/
//        Student student = liteConnection.select(Student.class).all().getSingleResult();
//         System.out.println(student.toString());

//        Student student = Student.builder().id(5).mssv("1712000").name("Nguyễn Văn A").dob("1999").build();

        /** Thao tác insert **/
//        liteConnection.insert(student).run();
//        List<Student> students = liteConnection.select(Student.class).all().getListResults();
//        students.forEach(x -> System.out.println(x.toString()));

        /** Thao tác update **/
//        student.setMssv("1712999");
//        student.setName("Tran Thi B");
//        liteConnection.update(student).where("id = 5").run();
//        List<Student> students = liteConnection.select(Student.class).all().getListResults();
//        students.forEach(x -> System.out.println(x.toString()));

        /** Thao tác delete **/
//        liteConnection.delete(Student.class).where("id = 5").run();
//        List<Student> students = liteConnection.select(Student.class).all().getListResults();
//        students.forEach(x -> System.out.println(x.toString()));

        /** Thao tác select có where **/
//        Student student = liteConnection.select(Student.class).all().where("name = 'Võ Trọng Púc'").getSingleResult();
//        System.out.println(student.toString());

        /** Thao tác select có group by **/
//        List<Employee> employees = liteConnection.select(Employee.class).all().where("company_id <= 3 ").groupBy(new String[]{"id", "company_id"}).getListResults(); // select * from person;
//        employees.forEach(x -> System.out.println(x.toString()));

        /** Thao tác select có having **/
//        List<Employee> employees = liteConnection.select(Employee.class).all().where("company_id <= 3").groupBy(new String[]{"id", "company_id"}).having("COUNT(*) >= 1 ").getListResults(); // select * from person;
//        employees.forEach(x -> System.out.println(x.toString()));

        /** Thực thi 1 câu sql query từ client **/
//        List<Student> students = (List<Student>) liteConnection.executeQuery("select * from Student limit 2;");
//        System.out.println(students.get(0).toString());

        /** Thao tác select với OneToOne mapping **/
        /** VD: 1 Person có 1 Address tương ứng **/
//        Person person = liteConnection.select(Person.class).all().getSingleResult();
//        System.out.println(person.toString());
//        System.out.println(person.getAddress().toString());

        /** Thao tac select voi OneToMany mapping **/
        /** Vd: 1 Company có nhiều nhân viên **/
//        Company company = liteConnection.select(Company.class).all().getSingleResult(); // select * from person;
//        System.out.println(company.toString());
//        System.out.println(company.getEmployeeList().get(0));

        /** Thao tac select voi OneToMany mapping **/
        /** Vd: 1 Person có nhiều car => 1 car có 1 owner **/
//        List<Car> cars = liteConnection.select(Car.class).all().getListResults();
//        cars.forEach(x -> System.out.println(x.toString()));

        commandClose.execute();
    }
}
