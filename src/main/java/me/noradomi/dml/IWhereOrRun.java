package me.noradomi.dml;

import java.sql.SQLException;
import java.util.List;

public interface IWhereOrRun<T> {
    IGroupByOrRun<T> where(String condition);
    List<T> getListResults() throws SQLException;
    T getSingleResult() throws SQLException;
    ILimitOrRun<T> limit(Integer limit);
}
