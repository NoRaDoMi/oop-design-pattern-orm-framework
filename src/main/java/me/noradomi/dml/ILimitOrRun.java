package me.noradomi.dml;

import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import java.util.List;

public interface ILimitOrRun<T> {
    IRun<T> limit(int limit) throws InvocationTargetException, IllegalAccessException;
    List<T> getListResults() throws SQLException;
    T getSingleResult() throws SQLException;

}
