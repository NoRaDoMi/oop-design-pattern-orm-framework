package me.noradomi.dml;

import me.noradomi.connection.LiteConnection;
import me.noradomi.utils.Utilities;

import java.sql.SQLException;

public class InsertSqlStatement<T> extends SqlStatement<T> implements IRunNonQuery {
    public InsertSqlStatement(LiteConnection liteConnection, T obj) {
        this.type = (Class<T>) obj.getClass();
        this.liteConnection = liteConnection;
        try {

            this.statement = "INSERT INTO " + mapper.getTableName(this.type) +
                    " (" + Utilities.getAttributeName(obj) + ")" + " VALUES " +
                    " (" + Utilities.getAttributeValue(obj) + ");";
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public int run() throws SQLException, IllegalAccessException {
        return executeNonQuery();
    }
}
