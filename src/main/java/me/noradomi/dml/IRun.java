package me.noradomi.dml;

import java.sql.SQLException;
import java.util.List;

public interface IRun<T> {
    List<T> getListResults() throws SQLException;
    T getSingleResult() throws SQLException;

}
