package me.noradomi.dml;

import java.sql.SQLException;

public interface IRunNonQuery {
    int run() throws SQLException, IllegalAccessException;
}
