package me.noradomi.dml;

import me.noradomi.connection.LiteConnection;

import java.sql.SQLException;

public class DeleteSqlStatement<T> extends SqlStatement<T> implements IWhereOrRunNonQuery, IRunNonQuery {
    public DeleteSqlStatement(LiteConnection conn, Class<T> type) {
        this.liteConnection = conn;
        this.type = type;

        try {
            this.statement = "DELETE FROM " + mapper.getTableName(type);
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println(this.statement);
    }

    @Override
    public IRunNonQuery where(String condition) {
        this.statement += " WHERE " + condition;
        return this;
    }

    @Override
    public int run() throws SQLException {
        this.statement += ";";
        System.out.println(this.statement);
        return executeNonQuery();
    }
}
