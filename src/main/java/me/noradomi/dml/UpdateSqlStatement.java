package me.noradomi.dml;

import me.noradomi.connection.LiteConnection;
import me.noradomi.utils.Utilities;

import java.sql.SQLException;

public class UpdateSqlStatement<T> extends SqlStatement<T> implements IWhereOrRunNonQuery, IRunNonQuery {

    public UpdateSqlStatement(LiteConnection conn, T obj) {
        this.type = (Class<T>) obj.getClass();
        this.liteConnection = conn;
        try {
            this.statement = "UPDATE " +  mapper.getTableName(this.type) +
                    " SET " + Utilities.getCoupleOfAttributes(obj);
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println(statement);
    }

    @Override
    public IRunNonQuery where(String condition) {
        this.statement += " WHERE " + condition;
        return this;
    }

    public int run() throws SQLException {
        this.statement += ";";
        System.out.println(this.statement);
        return executeNonQuery();
    }


}
