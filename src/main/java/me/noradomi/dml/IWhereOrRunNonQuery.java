package me.noradomi.dml;

import java.sql.SQLException;

public interface IWhereOrRunNonQuery {
    IRunNonQuery where(String condition);
    int run() throws SQLException;
}
