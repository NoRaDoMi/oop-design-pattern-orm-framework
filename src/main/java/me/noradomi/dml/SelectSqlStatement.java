package me.noradomi.dml;

import me.noradomi.connection.LiteConnection;

import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import java.util.List;


public class SelectSqlStatement<T> extends SqlStatement<T> implements IFilter<T>, IWhereOrRun<T>, IHavingOrRun<T>, IGroupByOrRun<T>, ILimitOrRun<T>, IRun<T> {
    public SelectSqlStatement(LiteConnection liteConnection, Class<T> type) {
        this.liteConnection = liteConnection;
        this.type = type;
        try {
            this.statement = "SELECT ";
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public IWhereOrRun<T> filter(String[] filter) throws InvocationTargetException, IllegalAccessException {
        this.statement += String.join(", ", filter) + " FROM " + mapper.getTableName(this.type);
        return this;
    }

    @Override
    public IWhereOrRun<T> all() throws InvocationTargetException, IllegalAccessException {
//        this.statement += String.join(", ", mapper.getColumns(type)) + " FROM " + mapper.getTableName(this.type);
        this.statement += "*" + " FROM " + mapper.getTableName(this.type);
        return this;
    }

    @Override
    public IGroupByOrRun<T> where(String condition) {
        this.statement += " WHERE " + condition;
        return this;
    }

    @Override
    public IHavingOrRun<T> groupBy(String[] columns) {
        this.statement += " GROUP BY " + String.join(", ", columns);
        return this;
    }

    @Override
    public ILimitOrRun<T> having(String condition) {
        this.statement += " HAVING " + condition;
        return this;
    }

    @Override
    public IRun<T> limit(int limit) throws InvocationTargetException, IllegalAccessException {
        this.statement += " LIMIT " + limit;
        return this;
    }

    @Override
    public ILimitOrRun<T> limit(Integer limit) {
        this.statement += " LIMIT "+limit;
        return this;
    }


    @Override
    public List<T> getListResults() throws SQLException {
        this.statement += ";";
        return executeQuery();
    }

    @Override
    public T getSingleResult() throws SQLException {
        this.statement += ";";
        return (T) executeQuery().get(0);
    }
}
