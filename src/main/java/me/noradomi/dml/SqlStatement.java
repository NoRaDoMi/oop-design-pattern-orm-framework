package me.noradomi.dml;

import me.noradomi.connection.LiteConnection;
import me.noradomi.mapper.EntityMapper;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class SqlStatement<T> {
    protected Class<T> type;
    protected String statement; // câu lệnh sql
    protected LiteConnection liteConnection;
    protected EntityMapper mapper = EntityMapper.getInstance();

    public <T> List<T> executeQuery() throws SQLException {
        Statement st = liteConnection.getConnection().createStatement();
        ResultSet rs = st.executeQuery(statement);
        List<T> result = new ArrayList<>();
        try {
            result = (List<T>) mapper.mappingToObject(rs, liteConnection, type);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public int executeNonQuery() throws SQLException {
        Statement st = liteConnection.getConnection().createStatement();
        return st.executeUpdate(statement);
    }

}
