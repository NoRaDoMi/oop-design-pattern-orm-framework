package me.noradomi.dml;

import java.lang.reflect.InvocationTargetException;

public interface IFilter<T> {
    IWhereOrRun<T> filter(String[] filter) throws InvocationTargetException, IllegalAccessException;
    IWhereOrRun<T> all() throws InvocationTargetException, IllegalAccessException;
}
