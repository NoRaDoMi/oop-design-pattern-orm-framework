package me.noradomi.utils;

import me.noradomi.mapper.EntityMapper;
import me.noradomi.mapper.annotations.Column;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

public class Utilities {

    private static List<FieldInfo> getFieldValue(Object obj) throws IllegalAccessException {
        Field[] fields = obj.getClass().getDeclaredFields();
        for (Field field : fields) {
            field.setAccessible(true);
        }
        List<FieldInfo> result = new ArrayList<>();

        for (int i = 0; i < fields.length; i++) {
            if(fields[i].isAnnotationPresent(Column.class)){
                FieldInfo fieldInfo = new FieldInfo(fields[i].getName(), fields[i].get(obj));
                result.add(fieldInfo);
            }
        }
        return result;
    }

    public static String getAttributeName(Object obj) {
        StringBuilder fieldName = new StringBuilder();
        Class<?> zclass = obj.getClass();
        for (Field field : EntityMapper.getAllFields(zclass)) {
            if(field.isAnnotationPresent(Column.class)){
                Column column = field.getAnnotation(Column.class);
                fieldName.append(column.columnName()).append(", ");
            }
        }
        fieldName = new StringBuilder(fieldName.substring(0, fieldName.length() - 2));
        return fieldName.toString();
    }

    public static String getAttributeValue(Object obj) throws IllegalAccessException {
        StringBuilder fieldValue = new StringBuilder();

        List<FieldInfo> fieldInfos = Utilities.getFieldValue(obj);
        for (FieldInfo fieldInfo : fieldInfos) {
            if (fieldInfo.getValue() instanceof String) {
                fieldValue.append("'").append(fieldInfo.getValue()).append("', ");
            } else {
                fieldValue.append(fieldInfo.getValue()).append(", ");
            }
        }
        fieldValue = new StringBuilder(fieldValue.substring(0, fieldValue.length() - 2));
        return fieldValue.toString();
    }

    public static String getCoupleOfAttributes(Object obj) throws IllegalAccessException {
        StringBuilder statement = new StringBuilder();

        List<FieldInfo> fieldInfos = Utilities.getFieldValue(obj);
        for (FieldInfo fieldInfo : fieldInfos) {
            if(fieldInfo.getValue() != null) {
                if (fieldInfo.getValue() instanceof String) {
                    statement.append(fieldInfo.getName()).append(" = '").append(fieldInfo.getValue()).append("', ");
                }
                else {
                    statement.append(fieldInfo.getName()).append(" = ").append(fieldInfo.getValue()).append(", ");
                }
            }
        }
        statement = new StringBuilder(new StringBuilder(statement.substring(0, statement.length() - 2)));
        return statement.toString();
    }

    public static String parseClassName(String className) {
        String keyword = "from";
        int posStart = className.toLowerCase().indexOf(keyword) + keyword.length() + 1;
        if(posStart < 0) {
            return "";
        }
        String temp = className.substring(posStart);
        int i = 0;
        while (temp.charAt(i) == ' ') {
            i++;
        }
        posStart += i;
        temp = temp.trim();
        int posEnd = temp.indexOf(' ');
        if(posEnd < 0) {
            posEnd = temp.length() - 1;
        }
        return className.substring(posStart, posStart + posEnd + 1).trim();
    }
}