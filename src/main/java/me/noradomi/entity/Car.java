package me.noradomi.entity;

import lombok.*;
import me.noradomi.mapper.annotations.*;

import java.io.Serializable;

@Builder
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "car")
public class Car implements Serializable {
    @Column(columnName = "id")
    private int id;

    @Column(columnName = "name")
    private String name;

    @ManyToOne(joinColumnName = "owner")
    private Person owner;

    @Override
    public String toString() {
        return "Car{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", owner=" + owner.toString() +
                '}';
    }
}
