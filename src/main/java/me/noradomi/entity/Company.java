package me.noradomi.entity;

import lombok.*;
import me.noradomi.mapper.annotations.Column;
import me.noradomi.mapper.annotations.OneToMany;
import me.noradomi.mapper.annotations.Table;

import java.util.ArrayList;
import java.util.List;

@Builder
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "company")
public class Company {
    @Column(columnName = "id")
    private int id;

    @Column(columnName = "name")
    private String name;

    @OneToMany(joinColumnName = "company_id")
    private List<Employee> employeeList = new ArrayList<>();

    @Override
    public String toString() {
        return "Company{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", employeeList=" + employeeList.toString() +
                '}';
    }
}
