package me.noradomi.entity;

import lombok.*;
import me.noradomi.mapper.annotations.Column;
import me.noradomi.mapper.annotations.OneToOne;
import me.noradomi.mapper.annotations.Table;

import java.io.Serializable;

@Builder
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "student")
public class Student implements Serializable {
    @Column(columnName = "id")
    private int id;

    @Column(columnName = "name")
    private String name;

    @Column(columnName = "mssv")
    private String mssv;

    @Column(columnName = "dob")
    private String dob;

    @Override
    public String toString() {
        return "Student{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", mssv='" + mssv + '\'' +
                ", dob='" + dob + '\'' +
                '}';
    }
}
