package me.noradomi.entity;

import lombok.*;
import me.noradomi.mapper.annotations.Column;
import me.noradomi.mapper.annotations.Table;

@Builder
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "address")
public class Address {


    @Column(columnName = "id")
    private int id;

    @Column(columnName = "city")
    private String city;

    @Column(columnName = "district")
    private String district;

    @Override
    public String toString() {
        return "Address{" +
                "id='" + id + '\'' +
                ", city='" + city + '\'' +
                ", district='" + district + '\'' +
                '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }
}
