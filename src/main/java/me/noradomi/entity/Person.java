package me.noradomi.entity;

import lombok.*;
import me.noradomi.mapper.annotations.Column;
import me.noradomi.mapper.annotations.OneToOne;
import me.noradomi.mapper.annotations.Table;

import java.io.Serializable;

@Builder
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "person")
public class Person implements Serializable {


    @Column(columnName = "id")
    private int id;

    @Column(columnName = "name")
    private String name;

    @Column(columnName = "age")
    private int age;

    @OneToOne(joinColumnName = "city")
    private Address address;

    @Override
    public String toString() {
        return "Person{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", age=" + age +
                ", address=" + address.toString() +
                '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }
}
