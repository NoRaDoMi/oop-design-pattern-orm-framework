package me.noradomi.entity;

import lombok.*;
import me.noradomi.mapper.annotations.Column;
import me.noradomi.mapper.annotations.ManyToOne;
import me.noradomi.mapper.annotations.OneToOne;
import me.noradomi.mapper.annotations.Table;

@Builder
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "employee")
public class Employee   {
    @Column(columnName = "id")
    private int id;

    @Column(columnName = "name")
    private String names;


    @Override
    public String toString() {
        return "Employee{" +
                "id=" + id +
                ", names='" + names + '\'' +
                '}';
    }
}
