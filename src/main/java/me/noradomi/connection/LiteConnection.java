package me.noradomi.connection;

import me.noradomi.config.DbConfiguration;
import me.noradomi.dml.IFilter;
import me.noradomi.dml.IRunNonQuery;
import me.noradomi.dml.IWhereOrRunNonQuery;

import java.io.FileNotFoundException;
import java.sql.Connection;
import java.sql.SQLException;

public abstract class LiteConnection {
    protected Connection conn;
    protected DbConfiguration dbConfiguration;
    abstract public void loadConfig(String path);
    abstract public void open() throws SQLException, FileNotFoundException;
    abstract public void close() throws SQLException;

    public abstract Connection getConnection();

    public abstract <T> IFilter<T> select(Class<T> clazz);

    public abstract <T> IFilter<T> selectOne(Class<T> clazz);

    public abstract <T> IRunNonQuery insert(T obj) throws IllegalAccessException, SQLException;

   public abstract <T> IWhereOrRunNonQuery update(T obj) throws IllegalAccessException, SQLException;

   public abstract <T> IWhereOrRunNonQuery delete(Class<T> clazz);

   public abstract Object executeQuery(String query) throws SQLException, ClassNotFoundException, IllegalAccessException, InstantiationException;
}
