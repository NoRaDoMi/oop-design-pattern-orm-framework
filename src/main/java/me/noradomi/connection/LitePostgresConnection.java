package me.noradomi.connection;

import me.noradomi.config.DbConfiguration;
import me.noradomi.config.FileConfigLoader;
import me.noradomi.database.HikariCPDataSource;
import me.noradomi.dml.*;
import me.noradomi.entity.Person;
import me.noradomi.mapper.EntityMapper;
import me.noradomi.utils.Utilities;

import java.io.FileNotFoundException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

public class LitePostgresConnection extends LiteConnection {

    @Override
    public void loadConfig(String path) {
        try {
            dbConfiguration = FileConfigLoader.load(path, DbConfiguration.class);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
    @Override
    public void open() throws SQLException, FileNotFoundException {
        System.out.println("Opening connection !!!");
        conn = HikariCPDataSource.getDataSource(dbConfiguration).getConnection();
    }

    @Override
    public void close() throws SQLException {
        if (conn != null) {
            System.out.println("Closing connection !!!");
            conn.close();
        }
    }

    @Override
    public <T> IFilter<T> select(Class<T> clazz) {
        return new SelectSqlStatement(this, clazz);
    }

    @Override
    public <T> IFilter<T> selectOne(Class<T> clazz) {
        return null;
    }


    @Override
    public <T> IRunNonQuery insert(T obj) throws SQLException, IllegalAccessException {
        return new InsertSqlStatement(this, obj);
    }

    @Override
    public <T> IWhereOrRunNonQuery update(T obj) throws SQLException {
        return new UpdateSqlStatement(this, obj);
    }

    @Override
    public <T> IWhereOrRunNonQuery delete(Class<T> clazz) {
        return new DeleteSqlStatement(this, clazz);
    }
    // query with your own statement sql
    @Override
    public Object executeQuery(String queryStatement) throws SQLException, ClassNotFoundException, IllegalAccessException, InstantiationException {
        Statement st = this.getConnection().createStatement();
        if(!queryStatement.toLowerCase().contains("select")) {
            return st.executeUpdate(queryStatement);
        }
        ResultSet rs = st.executeQuery(queryStatement);
        Object result = new Object();

        String className = "me.noradomi.entity." + Utilities.parseClassName(queryStatement);
        try {
            result = EntityMapper.getInstance().mappingToObject(rs, this, (Class.forName(className).newInstance()).getClass());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    public Connection getConnection(){
        return conn;
    }
}
