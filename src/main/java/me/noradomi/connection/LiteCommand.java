package me.noradomi.connection;

import java.io.FileNotFoundException;
import java.sql.SQLException;

public interface LiteCommand {
    void execute() throws SQLException, FileNotFoundException;
}
