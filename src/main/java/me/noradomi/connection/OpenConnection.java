package me.noradomi.connection;

import java.io.FileNotFoundException;
import java.sql.SQLException;

public class OpenConnection implements LiteCommand {

    private LiteConnection liteConnection;
    public OpenConnection(LiteConnection liteConnection){
        this.liteConnection = liteConnection;
    }
    @Override
    public void execute() throws SQLException, FileNotFoundException {
        this.liteConnection.open();
    }
}
