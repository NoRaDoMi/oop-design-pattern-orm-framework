package me.noradomi.connection;

import java.sql.SQLException;

public class CloseConnection implements LiteCommand {

    private LiteConnection liteConnection;
    public CloseConnection(LiteConnection liteConnection){
        this.liteConnection = liteConnection;
    }

    @Override
    public void execute() throws SQLException {
        this.liteConnection.close();
    }
}
