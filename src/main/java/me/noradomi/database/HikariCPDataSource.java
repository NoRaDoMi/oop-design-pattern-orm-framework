package me.noradomi.database;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import me.noradomi.config.DbConfiguration;

import javax.sql.DataSource;

public class HikariCPDataSource {
    public static DataSource getDataSource(DbConfiguration config) {
        HikariConfig hikariConfig = new HikariConfig();
        hikariConfig.setDriverClassName(config.getDriver());
        hikariConfig.setJdbcUrl(config.getUrl());
        hikariConfig.setUsername(config.getUsername());
        hikariConfig.setPassword(config.getPassword());
        return new HikariDataSource(hikariConfig);
    }

    private HikariCPDataSource() {

    }
}
