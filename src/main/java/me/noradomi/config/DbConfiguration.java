package me.noradomi.config;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DbConfiguration {
    public String username = "";
    public String password = "";
    public String driver = "";
    public String url = "";

    public String getDriver() {
        return driver;
    }

    public String getUrl() {
        return  url;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }
}