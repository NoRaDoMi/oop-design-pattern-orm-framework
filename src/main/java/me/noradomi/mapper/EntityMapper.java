package me.noradomi.mapper;

import com.google.common.collect.ImmutableMap;
import me.noradomi.connection.LiteConnection;
import me.noradomi.mapper.annotationPresent.*;
import me.noradomi.mapper.annotations.Column;
import me.noradomi.mapper.annotations.OneToMany;
import me.noradomi.mapper.annotations.OneToOne;
import me.noradomi.mapper.annotations.Table;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class EntityMapper {
    private static final Map<Class, Class> primitive2Object =
            ImmutableMap.<Class, Class>builder()
                    .put(int.class, Integer.class)
                    .put(long.class, Long.class)
                    .put(double.class, Double.class)
                    .put(float.class, Float.class)
                    .put(boolean.class, Boolean.class)
                    .put(byte.class, Byte.class)
                    .put(char.class, Character.class)
                    .put(short.class, Short.class)
                    .put(String.class, String.class)
                    .build();

    private static volatile EntityMapper instance = null;

    private EntityMapper() {
    }

    public static EntityMapper getInstance() {
        if(instance == null) {
            synchronized (EntityMapper.class) {
                if(instance == null) {
                    instance = new EntityMapper();
                }
            }
        }
        return instance;
    }

    // lay tat ca cac field trong class
    public static List<Field> getAllFields(Class<?> type) {
        List<Field> fields = new ArrayList<>(Arrays.asList(type.getDeclaredFields()));

        Class<?> supperClass = type.getSuperclass();
        if (supperClass != null) {
            fields.addAll(getAllFields(supperClass));
        }

        return fields;
    }

    public <T> void loadResultSetIntoObject(ResultSet rst, LiteConnection conn, T obj) throws Exception {
        Class<?> zclass = obj.getClass();
        for (Field field : getAllFields(zclass)) {
            field.setAccessible(true);
            Class<?> type = field.getType();
            IAnnotation annotation = AnnotationFactory.getInstance().getAnnotation(field);
            if(annotation == null)
                return;
            Object value = annotation.getValue(field, rst, conn, type);
            field.set(obj, value);
        }
    }

    private boolean isPrimitive(Class<?> type) {
        return null != primitive2Object.get(type);
    }

    public static Class<?> boxPrimitiveClass(Class<?> type) {
        Class<?> objectType = primitive2Object.get(type);
        if (null != objectType) {
            return objectType;
        } else {
            throw new IllegalArgumentException("class '" + type.getName() + "' is not a primitive");
        }
    }


    //  Lấy tên bảng tương ứng với object, gọi khi thực hiện query
//  @Table(name = "person")
    public <T> String getTableName(Class<T> clazz) throws InvocationTargetException, IllegalAccessException {
        if (clazz.isAnnotationPresent(Table.class)) {
            Annotation annotation = clazz.getAnnotation(Table.class);
            Class<? extends Annotation> type = annotation.annotationType();
            for (Method method : type.getDeclaredMethods()) {
                Object value = method.invoke(annotation, (Object[]) null);
                if (method.getName().equals("name"))
                    return (String) value;
            }
        }
        return null;
    }

    public <T> List<String> getColumns(Class<T> clazz) {
        List<String> columns = new ArrayList<>();
        for (Field field : getAllFields(clazz)) {
            field.setAccessible(true);
            Column column = field.getAnnotation(Column.class);
            if (column == null) { // bỏ qua các fields không được đánh annotation
                continue;
            }
            columns.add(column.columnName());
        }
        return columns;
    }

    //  Mapping 1-1
    public <T> List<T> mappingToObject(ResultSet rst, LiteConnection conn, Class<T> zclazz) throws Exception {
        List<T> res = new ArrayList<>();
        while (rst.next()) {
            T obj = zclazz.newInstance();
            loadResultSetIntoObject(rst, conn, obj);
            res.add(obj);
        }
        return res;
    }

    //    Xử lí cho @OneToOne và @ManyToOne
    public static  <T> T mapToOne(LiteConnection connection, Object joinId, Class<T> tClass) throws SQLException, InvocationTargetException, IllegalAccessException {
        String condition = "id = " + joinId.toString();
        List<T> data = connection.select(tClass).all().where(condition).getListResults();
        return data.get(0);
    }

    //    Xử lí cho @OneToMany
    public static  <T> List<T> mapOneToMany(LiteConnection connection, Object id, String joinColumnName, Class<T> tClass) throws SQLException, InvocationTargetException, IllegalAccessException {
        String condition = joinColumnName + " = " + id.toString();
        List<T> data = connection.select(tClass).all().where(condition).getListResults();
        return data;
    }
}
