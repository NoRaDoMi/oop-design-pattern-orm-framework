package me.noradomi.mapper.annotationPresent;

import me.noradomi.connection.LiteConnection;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.sql.ResultSet;
import java.sql.SQLException;

public interface IAnnotation<T> {
    T getValue(Field field, ResultSet rst, LiteConnection conn, Class<T> clazz) throws SQLException, InvocationTargetException, IllegalAccessException;
}
