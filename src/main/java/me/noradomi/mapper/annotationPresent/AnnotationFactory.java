package me.noradomi.mapper.annotationPresent;

import me.noradomi.mapper.annotations.Column;
import me.noradomi.mapper.annotations.ManyToOne;
import me.noradomi.mapper.annotations.OneToMany;
import me.noradomi.mapper.annotations.OneToOne;

import java.lang.reflect.Field;

public class AnnotationFactory {
    private static volatile AnnotationFactory instance = null;
    private AnnotationFactory() {}
    public static AnnotationFactory getInstance() {
        if(instance == null) {
            synchronized (AnnotationFactory.class) {
                if(instance == null) {
                    instance = new AnnotationFactory();
                }
            }
        }
        return instance;
    }

    public IAnnotation getAnnotation(Field field) {
        if (field.isAnnotationPresent(Column.class)) {
            return new ColumnAnnotation();
        } else if (field.isAnnotationPresent(OneToOne.class)) {
            return new OneToOneAnnotation();
        } else if (field.isAnnotationPresent(OneToMany.class)) {
            return new OneToManyAnnotation();
        }
        else if (field.isAnnotationPresent(ManyToOne.class)) {
            return new ManyToOneAnnotation();
        }
        return null;
    }
}
