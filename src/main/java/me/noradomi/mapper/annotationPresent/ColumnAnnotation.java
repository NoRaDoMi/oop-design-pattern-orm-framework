package me.noradomi.mapper.annotationPresent;

import me.noradomi.connection.LiteConnection;
import me.noradomi.mapper.EntityMapper;
import me.noradomi.mapper.annotations.Column;

import java.lang.reflect.Field;
import java.sql.ResultSet;
import java.sql.SQLException;

public class ColumnAnnotation<T> implements IAnnotation<T> {
    @Override
    public T getValue(Field field, ResultSet rst, LiteConnection conn, Class<T> clazz) throws SQLException {
        Column column = field.getAnnotation(Column.class);
        return (T) rst.getObject(column.columnName(), EntityMapper.boxPrimitiveClass(clazz));
    }
}
