package me.noradomi.mapper.annotationPresent;

import me.noradomi.connection.LiteConnection;
import me.noradomi.mapper.EntityMapper;
import me.noradomi.mapper.annotations.OneToMany;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.ParameterizedType;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

public class OneToManyAnnotation<T> implements IAnnotation<T> {
    @Override
    public T getValue(Field field, ResultSet rst, LiteConnection conn, Class<T> clazz) throws SQLException, InvocationTargetException, IllegalAccessException {
        OneToMany column = field.getAnnotation(OneToMany.class);
        String joinColumnName = column.joinColumnName();
        Integer id = rst.getObject("id", Integer.class);
        ParameterizedType stringListType = (ParameterizedType) field.getGenericType();
        Class<?> tClass = (Class<?>) stringListType.getActualTypeArguments()[0];
        return (T) EntityMapper.mapOneToMany(conn, id, joinColumnName, tClass);
    }
}
