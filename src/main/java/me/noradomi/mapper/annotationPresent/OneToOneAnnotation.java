package me.noradomi.mapper.annotationPresent;

import me.noradomi.connection.LiteConnection;
import me.noradomi.mapper.EntityMapper;
import me.noradomi.mapper.annotations.OneToOne;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.sql.ResultSet;
import java.sql.SQLException;

public class    OneToOneAnnotation<T> implements IAnnotation<T> {
    @Override
    public T getValue(Field field, ResultSet rst, LiteConnection conn, Class<T> type) throws SQLException, InvocationTargetException, IllegalAccessException {
        OneToOne column = field.getAnnotation(OneToOne.class);
        Object joinId = rst.getObject(column.joinColumnName(), Integer.class);

        return EntityMapper.mapToOne(conn, joinId, type);
    }
}
