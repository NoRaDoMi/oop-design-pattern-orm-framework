package me.noradomi.mapper.attribute;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class PrimaryKeyAttribute {
    private String name;
    private boolean autoIncrease;

    public PrimaryKeyAttribute() {
    }

    public PrimaryKeyAttribute(String name, boolean autoIncrease) {
        this.name = name;
        this.autoIncrease = autoIncrease;
    }
}
